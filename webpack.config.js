var path = require('path');
// var webpack = require('webpack');

module.exports = function(paths) {
  let config = {
    entry: paths.entry,
    output: {
      path: path.resolve(__dirname, paths.output),
      filename: paths.filename
    },
    resolveLoader: {
      root: path.join(__dirname, 'node_modules')
    },
    module: {
      loaders: [
        {
          test: /\.(js|jsx)$/,
          loader: 'babel-loader',
          exclude: /node_modules/
        },
        {
          test: /\.json$/,
          loader: 'json'
        },
        {
          test: /\.styl$/,
          loader: 'style-loader!css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!stylus-loader?paths=node_modules/flex-grid-framework/'
        }
      ]
    },
    devServer: {
      historyApiFallback: true,
      noInfo: true
    }
  };

  /*
  if (process.env.NODE_ENV === 'production') {
    // config.devtool = 'source-map'
    // http://vuejs.github.io/vue-loader/workflow/production.html
    config.plugins = (config.plugins || []).concat([
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: '"production"'
        }
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
      new webpack.optimize.OccurenceOrderPlugin()
    ]);
  }
  */

  return config;
}
