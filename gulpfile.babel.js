import gulp from 'gulp';
import require_dir from 'require-dir';

require_dir('./gulp/tasks');

gulp.task('default', [
  'server',
  'client'
]);
