
export default {
  to: {
    dest: './build',

    server: {
      src: './src/server',
      dest: './build/server',
      watch: './src/server/**'
    },

    client: {
      src: './src/client/index.js',
      dest: './build/client',
      views: ['./src/client/index.jade'],
      watch: './src/client/**',
      webpack: {
        entry: './src/client/index.jsx',
        output: './build/client',
        filename: 'app.js'
      }
    }
  }
}
