import gulp from 'gulp';
import webpack from 'webpack-stream';
import run from 'run-sequence';
import rimraf from 'rimraf';

import path from '../paths';
import webpackConfig from '../../webpack.config.js';

gulp.task('client', ['client:build', 'client:watch']);

gulp.task('client:build', cb => {
  return run(
    'client:clean',
    ['client:webpack', 'client:views'],
    cb
  );
});

gulp.task('client:webpack', () => {
  return gulp.src(path.to.client.src)
    .pipe(webpack(webpackConfig(path.to.client.webpack)))
    .pipe(gulp.dest(path.to.client.dest));
});

gulp.task('client:views', () => {
  return gulp.src(path.to.client.views)
    .pipe(gulp.dest(path.to.client.dest));
});

gulp.task('client:watch', () => {
  return gulp.watch(path.to.client.watch, ['client:build']);
});

gulp.task('client:clean', cb => {
  return rimraf(path.to.client.dest, cb)
});
