import gulp from 'gulp';
import shell from 'gulp-shell';
import run from 'run-sequence';
import rimraf from 'rimraf';
import nodemon from 'gulp-nodemon';

import path from '../paths';

gulp.task('server', cb => {
  return run('server:build', 'server:start', cb);
});

gulp.task('server:build', cb => {
  return run('server:clean', 'server:babel', cb);
});


gulp.task('server:clean', cb => {
  return rimraf(path.to.server.dest, cb);
});

gulp.task('server:babel', shell.task([
  `babel ${path.to.server.src} --out-dir ${path.to.server.dest} --ignore=${path.to.server.ignore}`
]));

gulp.task('server:start', () => {
  nodemon({
    script: path.to.server.dest,
    watch: path.to.server.src,
    tasks: ['server:build'],
    env: { 'NODE_ENV': 'development' }
  });
});
