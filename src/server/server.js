import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import passport from 'passport';
import _ from 'lodash';

import env from './config';
import modules from './modules';

export default () => {

  const app = express();

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(methodOverride());
  app.use(passport.initialize());

  app.use(express.static(env.static));
  app.set('view engine', 'jade');
  app.set('views', env.static);

  if (app.get('env') === 'development') {
    app.use(morgan('dev'));
  }

  // load modules defined in the module config
  _.map(modules, module => app.use(module.path, module.router));

  return app;
}
