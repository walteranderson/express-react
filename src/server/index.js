import mongoose from 'mongoose';

import env from './config';
import server from './server';

const app = server();

mongoose.connect(env.DATABASE_URL);
mongoose.connection.on('error', () => {
  console.log('mongoDB connection error');
});

app.listen(env.SERVER_PORT, () => {
  console.log(`Express server listening on port ${env.SERVER_PORT} in ${app.get('env')} mode`);
});

export default app;
