import User from './user.model';
import auth from '../auth/auth.service';

export default {

  index(req, res) {
    User.find()
      .select('-__v -password')
      .exec(function(err, users) {
        if (err) return res.status(500).send(err);
        if (!users) return res.status(404);
        return res.json(users);
      });
  },

  create(req, res) {
    let newUser = new User(req.body);

    newUser.save((err, user) => {
      if (err) return res.status(500).send(err);

      let token = auth.signToken(user._id);
      return res.json({ token });
    });
  },

  me(req, res) {
    var userId = req.user._id;

    User.findById(userId)
      .select('-__v -password')
      .exec((err, user) => {
        if (err) return res.status(500).send(err);
        if (!user) return res.status(404);

        return res.json(user);
      });
  },

  show(req, res) {
    let id = req.params.id;

    User.findById(id)
      .select('-__v -password')
      .exec((err, user) => {
        if (err) return res.status(500).send(err);
        if (!user) return res.status(404);

        return res.json(user);
      });
  },

  destroy(req, res) {
    User.findByIdAndRemove(req.params.id)
      .exec((err) => {
        if (err) return res.status(500).send(err);

        return res.status(204).end();
      });
  },

  changePassword() {
    let id  = req.user._id;
    let oldPass = String(req.body.oldPassword);
    let newPass = String(req.body.newPassword);

    User.findById(id)
      .exec((err, user) => {
        if (! user.validPassword(oldPass)) {
          return res.status(403).end();
        }

        user.password = user.generateHash(newPass);
        user.save((err) => {
          if (err) return res.status(500).send(err);

          return res.status(200).end();
        });
      });
  }
}
