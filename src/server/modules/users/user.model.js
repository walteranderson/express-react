import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const UserSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: String,
  role: {
    type: String,
    default: 'user'
  }
});

/**
 * Presave hook for new users.
 * Generates a hash from their password.
 */
UserSchema.pre('save', function(next) {
  if (!this.isNew) return next();

  var hash = this.generateHash(this.password);
  this.password = hash;
  next();
});

UserSchema.methods = {
  generateHash(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  },

  validPassword(password) {
    return bcrypt.compareSync(password, this.password);
  }
};

export default mongoose.model('User', UserSchema);
