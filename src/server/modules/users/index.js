import { Router } from 'express';
import controller from './user.controller';
import auth from '../auth/auth.middleware';

const router = Router();

router.get('/', controller.index);

router.post('/', controller.create);

router.get('/me', auth.check(), controller.me);

router.put('/:id/password', controller.changePassword);

router.get('/:id', controller.show);

router.delete('/:id', controller.destroy);

export default router;
