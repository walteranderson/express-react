import Auth from './auth';
import Users from './users';

export default [

  //
  // API Endpoints
  //

  /**
   * Authentication
   */
  {
    path: '/api/auth',
    router: Auth
  },

  /**
   * Users
   */
  {
    path: '/api/users',
    router: Users
  },


  //
  // Client Side
  //

  {
    path: '/*',
    router(req, res) {
      res.render('index');
    }
  }
];
