import { Strategy } from 'passport-local';
import User from '../../users/user.model';

const fields = {
  usernameField: 'email',
  passwordField: 'password'
}

export default new Strategy(fields, (email, password, done) => {
  User.findOne({ email }, (err, user) => {
    if (err) {
      return done(err);
    }

    if (! user) {
      return done(null, false, { message: 'Incorrect email' });
    }

    if (! user.validPassword(password)) {
      return done(null, false, { message: 'Incorrect password' });
    }

    return done(null, user);
  });
});
