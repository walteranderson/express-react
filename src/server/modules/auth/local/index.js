import { Router } from 'express';
import passport from 'passport';
import auth from '../auth.service';
import LocalStrategy from './strategy';

const router = Router();

passport.use(LocalStrategy);

/**
 * Callback if a user is successfully authenticated.
 *
 * Generates a signed JWT token using the user ID
 * and adds it to the response body.
 */
function createToken(req, res) {
  let token = auth.signToken(req.user._id);

  res.status(200).json({ token });
}

router.post('/', passport.authenticate('local', { session: false }), createToken);

export default router;
