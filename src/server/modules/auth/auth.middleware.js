import env from '../../config';
import User from '../users/user.model';
import compose from 'composable-middleware';
import jwtValidator from 'express-jwt';

const validateJwt = jwtValidator({ secret: env.SESSION_SECRET });

/**
 * Validate the Authorization header using JWT.
 * Allow access_token query parameter to also be used.
 */
function httpValidator(req, res, next) {
  if (req.query && req.query.hasOwnProperty('access_token')) {
    req.headers.authorization = 'Bearer ' + req.query.access_token;
  }

  if (! req.headers.authorization) {
    return res.status(401).send();
  }

  validateJwt(req, res, next);
}

/**
 * Inject the authorized user's model instance into the
 * request object.
 */
function injectUser(req, res, next) {
  User.findById(req.user._id, (err, user) => {
    if (err) return next(err);
    if (!user) return res.send(401);

    req.user = user;
    next();
  });
}

export default {

  /**
   * Confirm the user is logged in
   *
   * - validate JWT
   * - inject user instance into the request
   */
  check() {
    return compose()
      .use(httpValidator)
      .use(injectUser);
  },

  /*
   * Compare the logged in user's role to what is required.
   *
   * - authenticate the user
   * - confirm the user's role meets the requirement
   */
  hasRole(roleRequired) {
    return compose()
      .use(this.check())
      .use((req, res, next) => {
        let userRole = env.user_roles.indexOf(req.user.role); 
        let requiredRole = env.user_roles.indexOf(roleRequired);

        if (userRole <= requiredRole) {
          return res.status(403).send('Not Authorized');
        }

        next();
      });
  }
};
