import jwt from 'jsonwebtoken';
import env from '../../config';

export default {
  /*
   * Return jwt token signed by the app secret
   */
  signToken(_id) {
    return jwt.sign(
      { _id },
      env.SESSION_SECRET,
      { expiresIn: '2 days' }
    );
  }
}
