import { Router } from 'express';
import LocalAuth from './local'

const router = Router();

// registers the local authentication strategy
router.use('/local', LocalAuth);

export default router;
