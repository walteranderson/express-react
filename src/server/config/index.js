import path from 'path';
import _ from 'lodash';

require('dotenv').config();

let config = {
  root: path.normalize(__dirname + '/../../'),
  static: path.normalize(__dirname + '/../../') + 'client',
  user_roles: ['user', 'admin']
};

export default _.merge(config, process.env);
