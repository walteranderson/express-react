import React, { Component } from 'react';
import { Link } from 'react-router';
import _ from 'lodash';

import styles from './Navbar.styl';

export default class Navbar extends Component {
  render() {
    let links;

    if (this.props.user) {
      links = (
        <ul>
          <li><Link to='/'>Home</Link></li>
          <li><Link to='/test'>Test</Link></li>
          <li><Link to='/logout'>Logout</Link></li>
        </ul>
      );

    } else {
      links = (
        <ul>
          <li><Link to='/'>Home</Link></li>
          <li><Link to='/login'>Login</Link></li>
        </ul>
      );
    }

    return (
      <nav className={styles.nav}>
        {links}
      </nav>
    );
  }
}
