import AuthActions from '../auth/actions';
import AuthStore from '../auth/store';

export default {

  loginRequired(nextState, replace) {
    if (! AuthStore.loggedIn()) {
      replace({
        pathname: '/login',
        state: { nextPathname: nextState.location.pathname }
      });
    }
  },

  guestRequired(nextState, replace) {
    if (AuthStore.loggedIn()) {
      replace({
        pathname: '/',
        state: { nextPathname: nextState.location.pathname }
      });
    }
  },

  logout(nextState, replace) {
    AuthActions.logout();
    replace({
      pathname: '/',
      state: { nextPathname: nextState.location.pathname }
    });
  }
}
