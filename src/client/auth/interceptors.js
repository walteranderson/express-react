import AuthStore from './store';

export default {
  request(config) {
    let token = AuthStore.getToken();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
  }
}
