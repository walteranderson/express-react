import Reflux from 'reflux';

let AuthActions = Reflux.createActions({
  login: { asyncResult: true },
  logout: {}
});

export default AuthActions;
