import http from 'axios';
import interceptors from './interceptors';

http.interceptors.request.use(interceptors.request);

const LOGIN_PATH = '/api/auth/local';
const USER_PATH = '/api/users/me';

export default {
  login(email, password) {
    return new Promise((resolve, reject) => {
      http.post(LOGIN_PATH, { email, password })
          .then((res) => resolve(res.data))
          .catch((err) => reject(err.data));
    });
  },

  user() {
    return new Promise((resolve, reject) => {
      http.get(USER_PATH)
          .then((res) => resolve(res.data))
          .catch((err) => reject(err.data));
    });
  }
}
