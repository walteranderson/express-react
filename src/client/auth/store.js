import Reflux from 'reflux';
import AuthActions from './actions';
import Api from './api';

let AuthStore = Reflux.createStore({
  listenables: AuthActions,

  init() {
    this.token = localStorage.getItem('token');
    this.error = false;
    this.loading = false;
    this.user = null;

    this.checkForUser();
  },

  getState() {
    return {
      loading: this.loading,
      error: this.error,
      user: this.user,
      loggedIn: this.loggedIn()
    };
  },

  getToken() {
    return this.token;
  },

  loggedIn() {
    return this.token !== null;
  },

  changed() {
    this.trigger(this.getState());
  },

  onLogin(email, password) {
    this.loading = true;
    this.changed();

    Api.login(email, password)
       .then(AuthActions.login.completed)
       .catch(AuthActions.login.failed);
  },

  onLoginCompleted(response) {
    localStorage.setItem('token', response.token);
    this.token = response.token;

    Api.user()
      .then((response) => {
        this.user = response;
        this.error = false;
        this.loading = false;
        this.changed();
      });
  },

  onLoginFailed(error) {
    localStorage.removeItem('token');
    this.token = null;
    this.error = error;
    this.loading = false;
    this.user = null;

    this.changed();
  },

  onLogout() {
    localStorage.removeItem('token');
    this.token = null;
    this.error = false;
    this.loading = false;
    this.user = null;

    this.changed();
  },

  checkForUser() {
    if (! this.token) return;

    Api.user()
      .then((user) => {
        this.user = user;
        this.changed();
      }).catch(AuthActions.login.failed);
  }
});

export default AuthStore;
