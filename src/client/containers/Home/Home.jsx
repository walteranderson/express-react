import React from 'react';
import { Link } from 'react-router';

class Index extends React.Component {
  render() {
    return (
      <div>
        <h1>Index</h1>
        <p>This is the home route.</p>
      </div>
    )
  }
}

export default Index;
