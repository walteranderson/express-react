import React from 'react';

class Test extends React.Component {
  render() {
    return (
      <div>
        <h1>Authenticated Route</h1>
        <p>This is to demonstrate an authenticated route.</p>
      </div>
    );
  }
}

export default Test;
