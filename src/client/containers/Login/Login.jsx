import React from 'react';
import ReactDOM from 'react-dom';

import AuthActions from '../../auth/actions';
import AuthStore from '../../auth/store';

export default class Login extends React.Component {

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props, context) {
    super(props, context);

    this.state = {
      error: null
    };

    this.unsubscribe = AuthStore.listen(
      this.onAuthChange.bind(this)
    );

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onAuthChange(auth) {
    this.setState({
      error: auth.error
    });

    if (auth.loggedIn) {
      this.context.router.push('/');
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    let email = ReactDOM.findDOMNode(this.refs.email).value;
    let password = ReactDOM.findDOMNode(this.refs.password).value;
    AuthActions.login(email, password);
  }

  render() {
    let error;
    if (this.state.error) {
      error = (
        <div className='state-error' style={{ paddingBottom: 16 }}>
          { this.state.error }
        </div>
      );
    }

    return (
      <div>
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          { error }
          <input ref='email'
            type='text'
            placeholder='Email Address'
          />
          <input ref='password'
            type='password'
            placeholder='Password'
          />
          <input type='submit' value='Submit'/>
        </form>
      </div>
    );
  }
}
