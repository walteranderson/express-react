import React from 'react';
import styles from './App.styl';
import { Navbar } from '../../components';

import AuthActions from '../../auth/actions';
import AuthStore from '../../auth/store';

export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loggedIn: false,
      user: null
    };

    this.unsubscribe = AuthStore.listen(
      this.onAuthChange.bind(this)
    );
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onAuthChange(auth) {
    this.setState({
      loggedIn: auth.loggedIn,
      user: auth.user
    });
  }

  render() {
    return (
      <div>
        <Navbar user={this.state.user} />
        <div className={styles.container}>
          {this.props.children}
        </div>
      </div>
    )
  }
}
