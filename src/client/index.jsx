import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import App from './containers/App/App.jsx';
import Home from './containers/Home/Home.jsx';
import Login from './containers/Login/Login.jsx';
import Test from './containers/Test.jsx';

import AuthUtil from './util/authenticated';

const root = document.getElementById('app');

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="login" component={Login} onEnter={AuthUtil.guestRequired} />
      <Route path="test" component={Test} onEnter={AuthUtil.loginRequired} />
      <Route path="logout" onEnter={AuthUtil.logout} />
    </Route>
  </Router>
), root);
