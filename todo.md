# Task Application

Extends Google Tasks.
Uses the core data structure of tasks and task lists and then builds on that to provide other features.


Core Features
-------------

### Google Tasks integration

- all tasks are required to have an associated google task ID
- sync individual items on CRUD actions
- sync the list on login or on request
    - potentially have something that runs every night to sync tasks
- keep other feature data out of the core google data so that syncing is easier; reference by ID


### Pomodoro timer

- have the timer available on each task
    - keep those statistics associated with the task
- have the timer available on the task list
    - have an option to rotate through each task in the list
    - associate the stats to the each individual task and have aggregrate data available at the list level


### Notification system

- visible notification in the corner of the screen
- have option for sound notifications
- hook into pomodoro timer
    - option for extending the current session by the length of that session
- snooze the notification for X minutes (configurable)


Backlog of ideas
----------------

- other sync-able task apps (todoist, etc)
- support for task-list/task templates (dynamic creation of a task-list and tasks based on a template)
- arbitrary tags for each task and task-list
- a "today" screen that shows tasks overdue and due today
- the concept of a sequential or parallel task list
    - modify the way the tasks are displayed
    - sequential: you can only see the next tasks you need to do
    - parallel: you can see and do all of the tasks in the list
